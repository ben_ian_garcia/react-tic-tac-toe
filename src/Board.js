import React, {Component} from 'react';

import Square from './Square'
import Table from './Table'

class Board extends Component {
  constructor(props){
	  super(props);
	  this.state = {
		  squares: Array(9).fill(null),
		  first: null,
		  xIsNext: true,
		  xTurnNo: 0,
		  oTurnNo: 0,
		  tableVisible: false,
		  winnerTable:[],
	  };
  }
  
  handleClick(i){
	  const squares = this.state.squares.slice();
	  if (calculateWinner(squares) || squares[i]){
		  return;
	  }
	  squares[i] = this.state.xIsNext ? 'X' : 'O';
	  
	  if (squares[i] === 'X'){
		  this.setState({
			  xTurnNo: this.state.xTurnNo + 1,
		  });
	  } else {
		  this.setState({
			  oTurnNo: this.state.oTurnNo + 1,
		  });
	  }

	  this.setState({
		  squares: squares,
		  xIsNext: !this.state.xIsNext,
	});
  }
  
  showTable(){
	  if (this.state.tableVisible === false){
		  this.setState({
			  tableVisible: true,
			  })
	  } else {
		  this.setState({
			  tableVisible: false,
			  })
	  }
  }
  
  playAgain(win) {
	  let game = this.props.game + 1
	  this.props.setPlayerVisible(true, win, game)
	  this.setState({
		  tableVisible: true,
		  squares: Array(9).fill(null),
		  xTurnNo: 0,
		  oTurnNo: 0,
		  winnerTable: win,
	  })
  }
  
  renderSquare(i) {
    return (<Square 
	  value={this.state.squares[i]}
	  onClick={() => this.handleClick(i)}
	  />
	);
  }

	setVisible(){
		this.props.setPlayerVisible(true)
	}

  render() {
	let status, turn, playagain, viewwinners;
	//let tableData = this.state.winnerTable
	let tableData = this.props.winnerData
	//let nextPlayer = (this.state.xIsNext ? 'X' : 'O')
	let totalTurns = this.state.xTurnNo + this.state.oTurnNo
	
	const winner = calculateWinner(this.state.squares);
	const {firstTurn} = this.props
	let {boardVisible} = this.props
	let {player1} = this.props
	let {player2} = this.props
		
	if (firstTurn === "X" && this.state.first === null){
	  this.setState({
		  xIsNext: true,
		  first: "X",
		  })
	} else if (firstTurn === "O" && this.state.first === null){
	  this.setState({
		  xIsNext: false,
		  first: "O",
		  })
	} 

	if (winner) {
		let winnerTurnNo, winnerName
		turn = null
		if (winner === 'X'){
			winnerTurnNo = this.state.xTurnNo
			winnerName = player1
		} else {
			winnerTurnNo = this.state.oTurnNo
			winnerName = player2
		}
		var winnerData = [
			{
				gameNo: this.props.game,
				playerName: winnerName,
				noOfMoves: winnerTurnNo,
				symbol: winner,
			},
		]
		status = winnerName + ' wins in ' + winnerTurnNo + ' moves';
		if (this.state.tableVisible === false){
			this.setState({tableVisible:true})
		}			
		tableData = tableData.concat(winnerData)	
		playagain = <button onClick={() => this.playAgain(tableData, this.props.game)}> Play Again </button>
	} else {
		turn = (this.state.xIsNext ? player1 : player2) + "'s turn(" + (this.state.xIsNext ? 'X' : 'O') + ')';
		status = null
		playagain = null
		if (totalTurns < 9){
			viewwinners = <button onClick={() => this.showTable()}> View Results </button>
		}
	}
	if (totalTurns === 9 && winner === null){
		status = 'Game Draw!'
		winnerData = [
			{
				gameNo: this.props.game,
				playerName: 'DRAW',
				noOfMoves: '-',
				symbol: '-',
			},
		]
		if (this.state.tableVisible === false){
			this.setState({tableVisible:true})
		}			
		tableData = tableData.concat(winnerData)	
		playagain = <button onClick={() => this.playAgain(tableData, this.props.game)}> Play Again </button>
	}
	
	
    if (boardVisible === true){
    return (
      <div className="game-info">
		<div className="board">
			<div className="turn">
				<p>{turn}</p>
			</div>
			<div className="board-row">
			  {this.renderSquare(0)}
			  {this.renderSquare(1)}
			  {this.renderSquare(2)}
			</div>
			<div className="board-row">
			  {this.renderSquare(3)}
			  {this.renderSquare(4)}
			  {this.renderSquare(5)}
			</div>
			<div className="board-row">
			  {this.renderSquare(6)}
			  {this.renderSquare(7)}
			  {this.renderSquare(8)}
			</div>
			<div className="status">{status}</div>
			<div className="play-again">
			  {playagain}
			  {viewwinners}
			 </div>
		 </div>
		 <div className="winner-info">
		   <Table tableVisible={this.state.tableVisible}
		          tableData={tableData} />
		 </div>
      </div>
    );
    } else {
		return null
	}
  }
}

  function calculateWinner(squares) {
	  const lines = [
		[0,1,2],
		[3,4,5],
		[6,7,8],
		[0,3,6],
		[1,4,7],
		[2,5,8],
		[0,4,8],
		[2,4,6],
	  ];
	  for (let i = 0; i < lines.length; i++) {
		  const [a, b, c] = lines[i];
		  if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]){
			  return squares[a];
		  }
	  }
	  return null;
  }

export default Board