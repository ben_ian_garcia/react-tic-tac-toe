import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import Player from './Player'

class Game extends Component {
  constructor(props) {
	  super(props);
	  this.state = {
		  history: [
			{squares: Array(9).fill(null),}
		  ],
		  xIsNext: true,
	  };
  }
	
  render() {
    return (
      <div className="game">
	    <h1> TIC TAC TOE </h1>
	    <div className="player-info">
		  <Player />
		</div>
      </div>
    );
  }
}

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
