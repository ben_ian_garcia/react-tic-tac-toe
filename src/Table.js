import React, {Component} from 'react';

class Table extends Component {
	render(){
		const {tableVisible} = this.props
		const {tableData} = this.props
		if (tableVisible === true) {
			return (
				<table>
					<tr>
						<th colspan="4"> GAME WINNERS </th>
					</tr>
					<tr>
						<th> Game No. </th>
						<th> Player Name </th>
						<th> No. of Moves</th>
						<th> Symbol </th>
					</tr>
					<TableBody tableData={tableData} />
				</table>
			)
		} else {
			return null
		}
	}
}

const TableBody = (props) => {
	const rows = props.tableData.map((row, index) => {
		return(
			<tr>
				<td> {row.gameNo} </td>
				<td> {row.playerName} </td>
				<td> {row.noOfMoves} </td>
				<td> {row.symbol} </td>
			</tr>
		)
	})
	
	return <tbody>{rows}</tbody>
}

export default Table