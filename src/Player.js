import React, {Component} from 'react';

import Board from './Board'
import Table from './Table'

class Player extends Component{
  constructor(props){
	  super(props);
	  this.state = {
		  firstTurn: '',
		  playerVisible: true,
		  boardVisible: false,
		  winnerTable: [],
		  game: 1,
		  player1: 'Player 1',
		  player2: 'Player 2',
		  tableVisible: false,
	  };
	  this.setVisible = this.setVisible.bind(this)
  }
  
  setFirstTurn = (str) => {
    this.setState({firstTurn: str})
	this.setState({boardVisible: true})
	this.setState({playerVisible: false})
  }
  
  setVisible(isVisible, w, g){
	  this.setState({
		  playerVisible: isVisible,
		  boardVisible: !isVisible,
		  winnerTable: w,
		  game: g,
	  });
  }
  
  handleChange(e){
	  this.setState({ [e.target.name]: e.target.value })
  }
  
  setDefaults(){
	  this.setState({
		player1D: "Player 1",
		player2D: "Player 2",
		player1: "Player 1",
		player2: "Player 2",
	})
  }
   
  showTable(){
	  if (this.state.tableVisible === false){
		  this.setState({
			  tableVisible: true,
			  })
	  } else {
		  this.setState({
			  tableVisible: false,
			  })
	  }
  }
  
  render(){
	let nextTurn, gameinfo, viewwinners
	let playerVisible = this.state.playerVisible;
	let firstTurn = this.state.firstTurn
	if (firstTurn === 'X'){
		nextTurn = 'O'
	} else if (firstTurn === 'O'){
		nextTurn = 'X'
	}
	if (this.state.game === 1 && this.state.boardVisible === false){
		gameinfo = (
			<div>
				<p> Game number: {this.state.game} </p>
				<p> Player 1 [X]: {this.state.firstTurn} 
					<input type="text" 
							placeholder="Player 1 Name" 
							onChange={ e => this.handleChange(e) } 
							name="player1"
							value={this.state.player1D}/> 
				</p>
				<p> Player 2 [O]: {nextTurn} 
					<input type="text" 
							placeholder="Player 2 Name" 
							onChange={ e => this.handleChange(e) } 
							name="player2"
							value={this.state.player2D}/>
				</p>
				<button onClick={() => this.setDefaults()}> Use Default Names </button>
			</div>
		)
	} else {
		viewwinners = <button onClick={() => this.showTable()}> View Results </button>
		gameinfo = (
			<div>
				<p> Game number: {this.state.game} </p>
					<p> {this.state.player1} : X </p>
					<p> {this.state.player2} : O </p>
			</div>
		)
	}
	// Return Statements
	if (playerVisible === true){
		return (
			<div className="game-info">
				<div className="choose-first">
					<p>{viewwinners}</p>
					{gameinfo}
					<p> Choose a Symbol to play first: </p>
					<button onClick={() => {this.setFirstTurn("X")}}> X </button>
					<button onClick={() => {this.setFirstTurn("O")}}> O </button>
				</div>
				<div className="winner-info">
					<Table tableVisible={this.state.tableVisible}
						 tableData={this.state.winnerTable} />
				</div>
			</div>
		);
	} else {
		return (
			<div>
			    {gameinfo}
				<Board firstTurn={this.state.firstTurn}
					   boardVisible={this.state.boardVisible}
					   winnerData = {this.state.winnerTable}
					   game = {this.state.game}
					   player1 = {this.state.player1}
					   player2 = {this.state.player2}
					   setPlayerVisible={this.setVisible.bind(this)} />
			</div>
		  );
			
	}
	}
}


export default Player